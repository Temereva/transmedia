var gulp = require('gulp');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var cssnano = require('gulp-cssnano');


gulp.task('sass', function () {
  return gulp
    .src('assets/scss/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer({
      browsers:  ['last 2 versions', '> 1%', 'Firefox ESR'],
      cascade: false
    }))
    .pipe(cssnano({zindex: false, reduceIdents: false, colormin: false}))
    .pipe(gulp.dest('./assets/css/'));
});


gulp.task('watch', function() {
  return gulp
    .watch('assets/scss/**/*.scss', gulp.series('sass'))
    // When there is a change,
    // log a message in the console
    .on('change', function(event) {
      console.log('File ' + event.path + ' was ' + event.type + ', running tasks...');
    });
});
