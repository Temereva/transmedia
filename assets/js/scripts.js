    jQuery(document).ready(function($) {    
        // Disable scroll when focused on a number input.
        $('form').on('focus', 'input[type=number]', function(e) {
            $(this).on('wheel', function(e) {
                e.preventDefault();
            });
        });
    
        // Restore scroll on number inputs.
        $('form').on('blur', 'input[type=number]', function(e) {
            $(this).off('wheel');
        });
    
        // Disable up and down keys.
        $('form').on('keydown', 'input[type=number]', function(e) {
            if ( e.which == 38 || e.which == 40 ) {
                e.preventDefault();
            }              
        });  
        
        // Tooltips
        $('.tooltip').mouseover(function() {            
            let suggestion = $(this).siblings('input').attr('data-info'),
                parent = $(this).closest('p').addClass('help'),
                infobox = parent.children('.tooltip-info');

            if (infobox[0]){ //check if the infobox already exists
                infobox.text(suggestion);
            } else {
                $('<span class="tooltip-info"></span>').appendTo(parent).text(suggestion);
            }
        });

        $('.tooltip').mouseout(function() {
            $(this).closest('p').removeClass('help');
        });        
        
        // Click to edit the 'readonly' input
        $('.edit').click(function() {
            $(this).parent().addClass('editing');

            let amount = $('#amount'),
                temp = amount.focus().val();

            amount.attr('readonly', false);

            // Reset the input value to place the cursor at the end of the input 
            amount.val('').val(temp);
        });

        // Update the button text
        function changeInput() {
            let newAmount = $('#amount').val();

            $('#submit').text(newAmount).formatCurrency();

            $('.click-to-edit').removeClass('editing'); 

            $('#amount').attr('readonly', true);          
        } 

        $('.click-to-edit input').blur(function() {            
            changeInput();          
        });

        // Detect Enter click in the amount input field
        $('.click-to-edit input').keypress(function(event) {
            let keycode = (event.keyCode ? event.keyCode : event.which);

            if (keycode == '13') {                
                changeInput();
                event.preventDefault();
                $('#amount').trigger('blur');
                return false;
            }
        });

         // Validate an input field (check if the field is not empty)
         function validate(current) {
            let value = current.val(),
                required = current.attr('required'),
                errorMessage = current.attr('data-error'),
                parent = current.parent();

            if (value == 0 && required == 'required'&&
                errorMessage !== false &&
                typeof errorMessage !== typeof undefined  &&
                current.attr('name') !== 'amount')
            {
                parent.addClass('has-error');
                current.attr('placeholder', errorMessage);
            } else if (value == 0 &&
                required == 'required' &&
                (errorMessage == false || typeof errorMessage == typeof undefined)  &&
                current.attr('name') !== 'amount')
            {
                parent.addClass('has-error');

            } else if( value == 0 &&
                required == 'required'&&
                (errorMessage !== false || typeof errorMessage !== typeof undefined) &&
                current.attr('name') == 'amount')
            {
                parent.addClass('has-error');

                let messagebox = parent.children('.error-message');                  
                
                if (messagebox[0]) { //check if the message box already exists
                    messagebox.text(errorMessage);
                } else {
                    $('<span class="error-message"></span>').appendTo(current.parent()).text(errorMessage);
                }

            } else if (value !== 0 ) {
                current.attr('placeholder', '');
                current.parent().removeClass('has-error');                    
            }
        }

        // Validate fields while filling up the form 
        $('input').focusout(function() {
            validate($(this));
        });
        
        // Remove the error signs when editing the field
        $('input').focusin(function() {
            let parent = $(this).parent();
            if (parent.hasClass('has-error') && $(this).attr('readonly') !=='readonly') {
                parent.removeClass('has-error');
            }
        });        
       
        // Validate te form on submit 
        $('#theform').on('submit', function() {
            $('input').each(function() {
                validate($(this));
            });
            
            let hasErrors = $('.has-error');

            if(hasErrors[0]) {
                return false; 
            }
        }); 
    });